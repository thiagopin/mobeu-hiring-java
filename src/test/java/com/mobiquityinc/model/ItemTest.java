package com.mobiquityinc.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    void getRatio() {
        Item item = Item.of(0, 10, 2);
        assertEquals(0.2, item.getRatio());
    }

    @Test
    void validate() {
        assertTrue(Item.of(0, 10, 2).isValid());
        assertTrue(Item.of(0, 100, 20).isValid());
        assertFalse(Item.of(0, 100, 120).isValid());
        assertFalse(Item.of(0, 100, -10).isValid());
    }
}