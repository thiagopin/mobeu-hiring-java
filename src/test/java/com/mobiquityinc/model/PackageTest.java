package com.mobiquityinc.model;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class PackageTest {

    @Test
    void isOverCapacity() {
        assertTrue(Package.of(5, Arrays.asList(Item.of(0, 10, 5))).isOverCapacity());
        assertTrue(Package.of(5, Arrays.asList(
                Item.of(0, 2, 5),
                Item.of(0, 5, 5))).isOverCapacity());

        assertFalse(Package.of(10, Arrays.asList(
                Item.of(0, 2, 5),
                Item.of(0, 5, 5))).isOverCapacity());
    }

    @Test
    void totalCost() {
        assertEquals(10.0, Package.of(5, Arrays.asList(
                Item.of(0, 2, 5),
                Item.of(0, 5, 5))).totalCost());
    }

    @Test
    void validate() {
        // Over capacity
        assertFalse(Package.of(5, Arrays.asList(
                Item.of(0, 2, 5),
                Item.of(0, 5, 5))).isValid());

        // Package with capacity higher than 100l
        assertFalse(Package.of(110, Arrays.asList(
                Item.of(0, 2, 5),
                Item.of(0, 5, 5))).isValid());
    }
}