package com.mobiquityinc.solver.bnb;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Package;
import com.mobiquityinc.parser.PackerReader;
import com.mobiquityinc.solver.bnb.SolverBranchBound;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SolverBranchBoundTest {

    @ParameterizedTest
    @MethodSource("providePackages")
    void solve(Package pack, double cost, double weight, String indexes) throws APIException {
        Package finalPack = new SolverBranchBound().solve(pack);

        assertEquals(cost, finalPack.totalCost(), 0.01);
        assertEquals(weight, finalPack.totalWeight(), 0.01);
        assertEquals(indexes, finalPack.showItems());
    }

    private static Stream<Arguments> providePackages() {
        try {
            PackerReader r = new PackerReader();
            List<Package> packages = r.read("src/test/resources/sample.txt");

            return Arrays.asList(
                    Arguments.of(packages.get(0), 76.0, 72.30, "4"),
                    Arguments.of(packages.get(1), 0.0, 0.0, "-"),
                    Arguments.of(packages.get(2), 148.0, 74.57, "2,7"),
                    Arguments.of(packages.get(3), 143.0, 26.12, "8,9")
            ).stream();
        } catch (APIException e) {
            throw new IllegalArgumentException("Invalid arguments");
        }
    }
}