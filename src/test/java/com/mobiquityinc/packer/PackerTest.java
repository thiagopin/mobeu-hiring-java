package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.solver.SolverType;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class PackerTest {

    @Test
    void branchAndBoundPack() throws APIException {
        String result = Packer.pack("src/test/resources/sample.txt", SolverType.BRANCH_N_BOUND);
        String expected = Arrays.asList("4", "-", "2,7", "8,9").stream().collect(Collectors.joining("\n"));

        assertEquals(expected, result);
    }

    @Test
    void chocoPack() throws APIException {
        String result = Packer.pack("src/test/resources/sample.txt", SolverType.CHOCO);
        String expected = Arrays.asList("4", "-", "2,7", "8,9").stream().collect(Collectors.joining("\n"));

        assertEquals(expected, result);
    }
}