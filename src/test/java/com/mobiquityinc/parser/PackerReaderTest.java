package com.mobiquityinc.parser;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Package;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PackerReaderTest {

    private PackerReader reader = new PackerReader();


    @ParameterizedTest
    @MethodSource("provideParsePackageLine")
    void parseLine(String line, Package pack) throws APIException {
        assertEquals(pack, reader.parseLine(line));
    }

    private static Stream<Arguments> provideParsePackageLine() {
        return Stream.of(
                Arguments.of("81 : (1,53.38,€45) (2,88.62,€98) ",
                        Package.of(81, Arrays.asList(Item.of(1, 53.38, 45),
                                Item.of(2, 88.62, 98)))),
                Arguments.of("8 : (1,15.3,€34)",
                        Package.of(8, Arrays.asList(Item.of(1, 15.3, 34))))
        );
    }

    @Test
    void invalidPackagePattern() {
        assertThrows(APIException.class, () -> reader.parseLine("81 : (1,53.38,€45) : (2,88.62,€98) "));
        assertThrows(APIException.class, () -> reader.parseLine("81 : (1,53.38,€45) , (2,88.62,€98) "));
        assertThrows(APIException.class, () -> reader.parseLine("81 : 1,53.38,€45 ; (2,88.62,€98) "));
        assertThrows(APIException.class, () -> reader.parseLine("81 : 1,53.38,€45 2,88.62,€98 "));
        assertThrows(APIException.class, () -> reader.parseLine("81 : (1, 53.38, €45) (2, 88.62, €98)"));
    }

    @ParameterizedTest
    @MethodSource("provideParseItemData")
    void parseData(String data, Item item) throws APIException {
        assertEquals(item, reader.parseData(data));
    }

    private static Stream<Arguments> provideParseItemData() {
        return Stream.of(
                Arguments.of(" (1,85.31,€29)", Item.of(1, 85.31, 29)),
                Arguments.of(" (5, 46.81,$36) ", Item.of(5, 46.81, 36)),
                Arguments.of("(3,78.48,€3) ", Item.of(3, 78.48, 3)),
                Arguments.of("(8,19.36, R$79)", Item.of(8, 19.36, 79))
        );
    }

    @Test
    void invalidItemPattern() {
        assertThrows(APIException.class, () -> reader.parseData("8,19.36,€79"));
        assertThrows(APIException.class, () -> reader.parseData("8,€19.36,€79"));
        assertThrows(APIException.class, () -> reader.parseData("8,19,36,$79"));
    }

    @Test
    void read() throws APIException {
        List<Package> packages = reader.read("src/test/resources/sample.txt");
        assertEquals(4, packages.size());
    }
}