package com.mobiquityinc.model;

import com.mobiquityinc.exception.ConstraintException;

import java.util.Objects;

public class Item implements Validator {

    private int index;

    private double weight, cost;

    private Item(int index, double weight, double cost) {
        this.index = index;
        this.weight = weight;
        this.cost = cost;
    }

    public static Item of(int index, double weight, double cost) {
        return new Item(index, weight, cost);
    }

    public int getIndex() {
        return index;
    }

    public double getWeight() {
        return weight;
    }

    public double getCost() {
        return cost;
    }

    public double getRatio() {
        return cost / weight;
    }

    @Override
    public void validate() throws ConstraintException {
        if (this.weight > 100 || this.cost > 100) {
            throw new ConstraintException("Max weight and cost of an item is 100");
        }

        if (!(this.weight >= 0 && this.cost >= 0)) {
            throw new ConstraintException("Weight and cost must be greater than equal 0");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return index == item.index &&
                Double.compare(item.weight, weight) == 0 &&
                Double.compare(item.cost, cost) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(index, weight, cost);
    }

    @Override
    public String toString() {
        return String.format("(%d,%.2f,€%.2f)", this.index, this.weight, this.cost);
    }
}
