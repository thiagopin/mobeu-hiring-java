package com.mobiquityinc.model;

import com.mobiquityinc.exception.ConstraintException;

public interface Validator {

    void validate() throws ConstraintException;

    default boolean isValid() {
        try {
            validate();
            return true;
        } catch (ConstraintException e) {
            return false;
        }
    }
}
