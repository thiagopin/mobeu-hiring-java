package com.mobiquityinc.model;

import com.mobiquityinc.exception.ConstraintException;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Package implements Validator {

    private int capacity;

    private List<Item> items;

    private Package(int capacity, List<Item> items) {
        this.capacity = capacity;
        this.items = Collections.unmodifiableList(items);
    }

    public static Package of(int capacity, List<Item> items) {
        return new Package(capacity, items);
    }

    public static Package empty(int capacity) {
        return new Package(capacity, Collections.emptyList());
    }

    public int getCapacity() {
        return capacity;
    }

    public List<Item> getItems() {
        return items;
    }

    public int numItems() {
        return items.size();
    }

    public boolean isOverCapacity() {
        return totalWeight() > capacity;
    }

    public double totalWeight() {
        return items.stream().mapToDouble(Item::getWeight).sum();
    }

    public double totalCost() {
        return items.stream().mapToDouble(Item::getCost).sum();
    }

    public String showItems() {
        return this.items.isEmpty() ? "-" : this.items.stream()
                .map(it -> Integer.toString(it.getIndex()))
                .collect(Collectors.joining(","));
    }

    @Override
    public void validate() throws ConstraintException {
        if (isOverCapacity()) throw new ConstraintException("Items exceeded package capacity.");
        if (this.capacity > 100) throw new ConstraintException("Max weight that a package can take is 100");
        if (this.items.size() > 15)
            throw new ConstraintException("There might be up to 15 items you need to choose from");
        for (Item item : this.items) {
            item.validate();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Package aPackage = (Package) o;
        return capacity == aPackage.capacity &&
                Objects.equals(items, aPackage.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(capacity, items);
    }

    @Override
    public String toString() {
        String cost = String.format("%.2f", this.totalCost());
        String cap = String.format("%.2f/%d", this.totalWeight(), this.capacity);

        return String.format("%s\n%s\n\n%s", cost, cap, this.showItems());
    }
}
