package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Package;
import com.mobiquityinc.parser.PackerReader;
import com.mobiquityinc.solver.Solver;
import com.mobiquityinc.solver.SolverFactory;
import com.mobiquityinc.solver.SolverType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Packer {

    /**
     * Read input file and solves the problem using the Branch and Bound Solver.
     *
     * @param filePath
     * @return
     * @throws APIException
     */
    public static String pack(String filePath) throws APIException {
        return pack(filePath, SolverType.BRANCH_N_BOUND);
    }

    /**
     * Read the input file and solves the problem using an specified Solver
     *
     * @param filePath
     * @param solverType
     * @return
     * @throws APIException
     */
    public static String pack(String filePath, SolverType solverType) throws APIException {
        return packAll(filePath, solverType).stream().map(Package::showItems)
                .collect(Collectors.joining("\n"));
    }

    public static List<Package> packAll(String filePath, SolverType solverType) throws APIException {
        PackerReader reader = new PackerReader();
        Solver solver = SolverFactory.createSolver(solverType);

        List<Package> packages = reader.read(filePath);
        List<Package> solvedPackages = new ArrayList<>();
        for (Package pack : packages) {
            solvedPackages.add(solver.solve(pack));
        }

        return solvedPackages;
    }
}
