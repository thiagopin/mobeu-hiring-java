package com.mobiquityinc.parser;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.exception.ConstraintException;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Package;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class responsible for parsing input file data.
 */
public class PackerReader {

    /**
     * Convert a file content into a List o Package
     *
     * @param filePath
     * @return list of Package objects.
     * @throws APIException if file is not on the expected format.
     */
    public List<Package> read(String filePath) throws APIException {
        try {
            List<String> lines = Files.readAllLines(Paths.get(filePath));
            List<Package> packages = new ArrayList<>();
            for (String line : lines) {
                packages.add(parseLine(line));
            }
            return packages;
        } catch (IOException e) {
            throw new APIException("Problem reading input file", e);
        }
    }

    /**
     * @param line input file line
     * @return package
     * @throws APIException
     */
    Package parseLine(String line) throws APIException {
        String[] info = line.trim().split(" : ");
        if (info.length != 2) {
            throw new APIException("Line don't match expected format: " + line);
        }

        int k = Integer.parseInt(info[0]);
        String[] lineItems = info[1].trim().split("\\s+");

        List<Item> items = new ArrayList<>();
        for (String data : lineItems) {
            Item item = parseData(data);
            items.add(item);
        }

        return Package.of(k, items);
    }

    Item parseData(String data) throws APIException {
        Pattern pattern = Pattern.compile("\\((\\d+),\\s*?(\\d+\\.?\\d+),\\s*.*?(\\d+)\\)");
        Matcher matcher = pattern.matcher(data.trim());
        if (matcher.find()) {
            int index = Integer.parseInt(matcher.group(1));
            double weight = Double.parseDouble(matcher.group(2));
            double cost = Double.parseDouble(matcher.group(3));
            return Item.of(index, weight, cost);
        } else {
            throw new APIException("No item pattern found on String " + data);
        }
    }

}
