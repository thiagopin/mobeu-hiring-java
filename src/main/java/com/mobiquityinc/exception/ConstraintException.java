package com.mobiquityinc.exception;

public class ConstraintException extends Exception {

  public ConstraintException(String message, Exception e) {
    super(message, e);
  }

  public ConstraintException(String message) {
    super(message);
  }
}
