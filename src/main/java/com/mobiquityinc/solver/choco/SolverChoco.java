package com.mobiquityinc.solver.choco;

import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Package;
import com.mobiquityinc.solver.Solver;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Uses Choco Solver (Constraint Programming Lib) for optimal item allocation.
 */
public class SolverChoco extends Solver {

    @Override
    public Package allocateItems(Package pack) {
        if (!pack.getItems().isEmpty()) {
            // Create Model object
            Model model = new Model("Knapsack");
            // Add occurrences variable
            BoolVar[] occurrences = model.boolVarArray(pack.numItems());
            // Max allowed weight: package capacity or 100 (the values are multiplied by 100, to get the double precision)
            IntVar maxWeight = model.intVar(0, 100 * Math.min(pack.getCapacity(), 100));
            // Max cost allowed: on the worst case the sum of all items costs
            IntVar maxCost = model.intVar(0, (int) pack.totalCost());
            // Objective value: Max cost have a higher relevancy and Max weight have a lower relevancy, then:
            // obj = 1000 * maxCost - maxWeight
            IntVar obj = maxCost.mul(1000).sub(maxWeight).intVar();

            // Items weight as array
            int[] weights = pack.getItems().stream().mapToInt(it -> (int) Math.round(100 * it.getWeight())).toArray();
            // Items costs as array
            int[] costs = pack.getItems().stream().mapToInt(it -> (int) it.getCost()).toArray();

            // Declaring a knapsack constraint
            model.knapsack(occurrences, maxWeight, maxCost, weights, costs).post();
            // Declaring a max occurrence of 15 items
            model.sum(occurrences, "<=", 15).post();

            // Set objective to the model
            model.setObjective(true, obj);

            // Solve and generate result object
            org.chocosolver.solver.Solver s = model.getSolver();

            Package finalPackage = Package.empty(pack.getCapacity());
            int bestValue = 0;
            while (s.solve()) {
                int actualValue = obj.getValue();
                if (actualValue > bestValue) {
                    List<Item> bestItems = IntStream.range(0, pack.numItems())
                            .filter(it -> occurrences[it].getValue() == 1)
                            .mapToObj(it -> pack.getItems().get(it))
                            .collect(Collectors.toList());

                    finalPackage = Package.of(pack.getCapacity(), bestItems);
                    bestValue = actualValue;
                }
            }
            return finalPackage;
        } else {
            return pack;
        }
    }
}
