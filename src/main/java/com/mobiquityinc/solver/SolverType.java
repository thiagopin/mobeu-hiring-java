package com.mobiquityinc.solver;

public enum SolverType {
    BRANCH_N_BOUND, CHOCO
}
