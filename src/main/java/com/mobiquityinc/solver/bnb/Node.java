package com.mobiquityinc.solver.bnb;

import com.mobiquityinc.model.Item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class Node implements Comparable<Node> {

    /**
     * Actual Node height
     */
    private int h;
    /**
     * List of parent nodes
     */
    private List<Item> taken;
    /**
     * Relaxed variable
     */
    private double bound;
    /**
     * Node value
     */
    private double value;
    /**
     * Node weight
     */
    private double weight;

    /**
     * @param items    all items available
     * @param capacity package capacity
     */
    Node(List<Item> items, int capacity) {
        this.h = 0;
        this.taken = Collections.emptyList();
        this.value = 0;
        this.weight = 0;

        this.bound = calculateBound(items, capacity);
    }

    /**
     * @param parent   parent Node
     * @param items    all items available
     * @param capacity package capacity
     */
    Node(Node parent, List<Item> items, int capacity) {
        this.h = parent.h + 1;
        this.taken = new ArrayList<>(parent.taken);
        this.value = parent.value;
        this.weight = parent.weight;

        this.bound = calculateBound(items, capacity);
    }

    /**
     * Increment actual node value by a given value
     *
     * @param value
     */
    void incrementValue(double value) {
        this.value += value;
    }

    /**
     * Increment actual weight value by a given weight
     *
     * @param weight
     */
    void incrementWeight(double weight) {
        this.weight += weight;
    }

    int getH() {
        return h;
    }

    double getBound() {
        return bound;
    }

    double getValue() {
        return value;
    }

    double getWeight() {
        return weight;
    }

    int numItems() {
        return this.taken.size();
    }

    /**
     * Add item to the list of selected items.
     *
     * @param item
     * @return
     */
    boolean add(Item item) {
        return taken.add(item);
    }

    /**
     * Convert selected items to a Stream of items
     *
     * @return
     */
    Stream<Item> itemStream() {
        return taken.stream();
    }

    /**
     * Calculate the relaxed variable
     *
     * @param items
     * @param capacity
     * @return
     */
    private double calculateBound(List<Item> items, int capacity) {
        int i = h;
        double w = weight;
        double bound = value;
        Item item = items.get(i);
        while (i < items.size()) {
            if (w + item.getWeight() > capacity) break;
            w += item.getWeight();
            bound += item.getCost();
            i++;
        }
        return bound + (capacity - w) * item.getRatio();
    }

    @Override
    public int compareTo(Node other) {
        return (int) (other.bound - bound);
    }

}