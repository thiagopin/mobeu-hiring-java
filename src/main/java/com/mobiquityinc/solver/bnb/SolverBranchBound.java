package com.mobiquityinc.solver.bnb;

import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Package;
import com.mobiquityinc.solver.Solver;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

/**
 * Uses a Branch and Bound Solver for optimal item allocation.
 */
public class SolverBranchBound extends Solver {

    private Package bnb(Package pack) {
        // Reverse order items by their ratios (values/weight).
        List<Item> items = pack.getItems().stream()
                .sorted(Comparator.comparing(Item::getRatio).reversed())
                .collect(Collectors.toList());

        int cap = Math.min(100, pack.getCapacity());

        Node best = new Node(items, cap);
        Node root = new Node(items, cap);

        // Explore nodes using their ratios as a ordering factor.
        PriorityQueue<Node> q = new PriorityQueue<>();
        q.offer(root);

        while (!q.isEmpty()) {
            Node node = q.poll();

            if (node.getBound() > best.getValue() && node.getH() < items.size() - 1) {

                Node with = new Node(node, items, cap);
                Item item = items.get(node.getH());
                with.incrementWeight(item.getWeight());

                // Max num of items must be 15
                if (with.getWeight() <= cap && with.numItems() <= 15) {

                    with.add(items.get(node.getH()));
                    with.incrementValue(item.getCost());

                    if (with.getValue() > best.getValue()) {
                        best = with;
                    }
                    if (with.getBound() > best.getValue()) {
                        q.offer(with);
                    }
                }

                Node without = new Node(node, items, cap);

                if (without.getBound() > best.getValue()) {
                    q.offer(without);
                }
            }
        }

        return Package.of(pack.getCapacity(),
                best.itemStream()
                        .sorted(Comparator.comparing(Item::getIndex))
                        .collect(Collectors.toList()));
    }

    @Override
    public Package allocateItems(Package pack) {
        if (!pack.getItems().isEmpty()) {
            return bnb(pack);
        } else {
            return pack;
        }
    }
}
