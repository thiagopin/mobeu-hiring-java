package com.mobiquityinc.solver;

import com.mobiquityinc.solver.bnb.SolverBranchBound;
import com.mobiquityinc.solver.choco.SolverChoco;

public class SolverFactory {

    public static Solver createSolver(SolverType type) {
        if (type == SolverType.BRANCH_N_BOUND) {
            return new SolverBranchBound();
        } else {
            return new SolverChoco();
        }
    }
}
