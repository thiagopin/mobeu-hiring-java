package com.mobiquityinc.solver;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.exception.ConstraintException;
import com.mobiquityinc.model.Package;

import java.util.stream.Collectors;

/**
 * The solver interface contains essential methods for the item allocation.
 * <p>
 * It's implementation must override the allocateItems method, this method contains the item allocation rules for
 * optimal allocation.
 */
public abstract class Solver {

    /**
     * Remove invalid items from package. Invalid items are the ones with weigh higher than the package support or
     * with weight or cost with values grater than 100.
     *
     * @param pack
     * @return
     */
    protected Package initialFilter(Package pack) {
        return Package.of(pack.getCapacity(),
                pack.getItems().stream()
                        .filter(it -> it.getWeight() <= pack.getCapacity() && it.isValid())
                        .collect(Collectors.toList()));
    }

    /**
     * This method is responsible to allocate the items in the package.
     * <p>
     * Uses the initialFilter method to remove the invalid items form the package and uses the remaining items as
     * initial data on the allocateItems method.
     *
     * @param pack
     * @return
     * @throws APIException
     */
    public Package solve(Package pack) throws APIException {
        try {
            Package finalPack = allocateItems(initialFilter(pack));
            finalPack.validate();
            return finalPack;
        } catch (ConstraintException e) {
            throw new APIException("Broken constraint exception", e);
        }
    }

    /**
     * Contains the item allocation rules for optimal allocation.
     *
     * @param pack
     * @return
     */
    public abstract Package allocateItems(Package pack);

}
