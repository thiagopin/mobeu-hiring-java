# Mobiquity Code Assignment: Package Challenge

The code assignment basically focus on the solution of the Knapsack problem, a classic problem on the Discrete
Optimization field.

I used two different approaches on the solution of this problem.

1. Using a Constraint Programming Library [Choco Solver](http://www.choco-solver.org/);
2. Using a Branch And Bound technique.

To use the Branch and Bound Solver, just use the staci method on the `Packer` class

```java
public static String pack(String filePath) throws APIException
```

To specify the Solver, use the method:

```java
public static String pack(String filePath, SolverType solverType) throws APIException
```

A `SolverFactory` was created to build both implementations.
For the model classes a immutable approach was used.

For easy execution, some tests are provided on the `PackerTest` class.
There are also more tests on the test directory.

